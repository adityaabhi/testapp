package com.abp.loginsample;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import java.util.Map;
import java.util.concurrent.TimeUnit;

import cp.sdk.BuildConfig;
import cp.sdk.CpVideoView;
import cp.sdk.IVideoView;

public class RegistrationActivity extends AppCompatActivity implements IVideoView
{

    EditText etEmail,etOTP;
    private String mVerificationId;
    private Button btnDone,btnOTP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_registration);


        CpVideoView mCpVideoView = new CpVideoView(this);
        mCpVideoView.start();





        etEmail = findViewById(R.id.et_email);
        etOTP = findViewById(R.id.et_otp);



    }


    @Override
    public View getView() {
        return null;
    }

    @Override
    public int getVideoWidth() {
        return 0;
    }

    @Override
    public int getVideoHeight() {
        return 0;
    }

    @Override
    public void setForceFullScreen(boolean b) {

    }

    @Override
    public void setAdsListener(AdsPlayListener adsPlayListener) {

    }

    @Override
    public void setOnPreparedListener(OnPreparedListener onPreparedListener) {

    }

    @Override
    public void setOnCompletionListener(OnCompletionListener onCompletionListener) {

    }

    @Override
    public void setOnErrorListener(OnErrorListener onErrorListener) {

    }

    @Override
    public void setOnSeekCompleteListener(OnSeekCompleteListener onSeekCompleteListener) {

    }

    @Override
    public void setOnInfoListener(OnInfoListener onInfoListener) {

    }

    @Override
    public void setOnBufferingUpdateListener(OnBufferingUpdateListener onBufferingUpdateListener) {

    }

    @Override
    public void setOnVideoSizeChangedListener(OnVideoSizeChangedListener onVideoSizeChangedListener) {

    }

    @Override
    public void setVideoUri(String s, Map<String, String> map) {

    }

    @Override
    public void start() {

    }

    @Override
    public void pause() {

    }

    @Override
    public int getDuration() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        return 0;
    }

    @Override
    public void seekTo(int i) {

    }

    @Override
    public boolean isPlaying() {
        return false;
    }

    @Override
    public boolean canPause() {
        return false;
    }

    @Override
    public boolean canSeekBackward() {
        return false;
    }

    @Override
    public boolean canSeekForward() {
        return false;
    }

    @Override
    public void stopPlayback() {

    }

    @Override
    public void setAccountInfo(String s, String s1) {

    }

    @Override
    public Bundle invoke(int i, Object... objects) {
        return null;
    }
}
