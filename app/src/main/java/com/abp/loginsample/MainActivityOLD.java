package com.abp.loginsample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivityOLD extends AppCompatActivity {



    Button mBtn;
    EditText etPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);


        mBtn = findViewById(R.id.btn_Register);
        etPhone = findViewById(R.id.et_phonenumber);



    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        //FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
    }



    public void clickRegister(View view)
    {
        if(etPhone.getText().toString().isEmpty())
        {
            Toast.makeText(this,"Enter Valid Number",Toast.LENGTH_LONG).show();
        }
        else
        {
            Intent intent = new Intent(this,RegistrationActivity.class);
            intent.putExtra("phone_number",etPhone.getText().toString());
            startActivity(intent);

        }
    }


}
