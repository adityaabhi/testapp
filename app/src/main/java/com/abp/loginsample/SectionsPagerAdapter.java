package com.abp.loginsample;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter
{
    ArrayList<String> dataList;
    private static final String ARG_SECTION_NUMBER = "section_number";
    private final Context mContext;
    public SectionsPagerAdapter(Context context, FragmentManager fm, ArrayList<String> list) {
        super(fm);
        mContext = context;
        dataList = list;

    }

    @Override
    public Fragment getItem(int position)
    {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).

        Bundle args = new Bundle();
        args.putStringArrayList("datalist",dataList);
        args.putInt(ARG_SECTION_NUMBER, position);
        PlaceholderFragment fragment = new PlaceholderFragment();
        fragment.setArguments(args);
        return fragment;
        //return PlaceholderFragment.newInstance(position + 1);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return  dataList.get(position);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return dataList.size();
    }
}