package com.abp.loginsample;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.ads.interactivemedia.v3.api.ImaSdkFactory;
import com.google.ads.interactivemedia.v3.api.ImaSdkSettings;
import com.google.ads.interactivemedia.v3.api.player.VideoAdPlayer;
import com.google.ads.interactivemedia.v3.api.player.VideoProgressUpdate;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader;
import com.google.android.exoplayer2.ext.ima.ImaAdsMediaSource;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.ads.AdsMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;


public class PlayerActivity extends Activity  {


    private PlayerView playerView;
    private SimpleExoPlayer player;
    private SpinKitView spin_kit;
    DataSource.Factory dataSourceFactory;
    ImaAdsLoader imaAdsLoader;
    private VideoAdPlayer mVideoAdPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exo_player_activity);




      // String adCode = "https://pubads.g.doubleclick.net/gampad/ads?iu=/2599136/Hindi_MITV_Preroll&description_url=https://www.abplive.in/&env=vp&impl=s&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=640x480&unviewed_position_start=1&hl=en";

       // String adCode = "https://pubads.g.doubleclick.net/gampad/ads?slotname=/124319096/external/ad_rule_samples&sz=640x480&ciu_szs=300x250&cust_params=deployment%3Ddevsite%26sample_ar%3Dpreonly&url=https://developers.google.com/interactive-media-ads/docs/sdks/android/tags&unviewed_position_start=1&output=xml_vast3&impl=s&env=vp&gdfp_req=1&ad_rule=0&vad_type=linear&vpos=preroll&pod=1&ppos=1&lip=true&min_ad_duration=0&max_ad_duration=30000&vrid=5776&video_doc_id=short_onecue&cmsid=496&kfa=0&tfcd=0";

      // String adCode = getString(R.string.ad_tag_url);

         String adCode = getString(R.string.ad_tag_url_new);// getting

       // String adCode = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=";
        imaAdsLoader = new ImaAdsLoader(this,Uri.parse(adCode));

        spin_kit = findViewById(R.id.spin_kit);
        playerView = findViewById(R.id.player_view);

    }

     public void onStart()
     {
         super.onStart();

        player = CreatePlayer(playerView, true);
        PlayStreamAds(player,getString(R.string.up_uk_channelURL));



    }

    private SimpleExoPlayer CreatePlayer(PlayerView playerViewToInitialize, boolean showControllerInPlayer)
    {
        // Grabs a reference to the playerView view
        playerViewToInitialize.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL); // Note that fill mode will distort the aspect ratio of the video,

        // 1. Create a default TrackSelector
        Handler mainHandler = new Handler();
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        // 2. Create the player
        SimpleExoPlayer playerToInitialize = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);

        // Bind the player to the view.
        // playerView.setUseController(false);
        playerViewToInitialize.setPlayer(playerToInitialize);
        playerViewToInitialize.setUseController(showControllerInPlayer);
        return playerToInitialize;
    }

    private void PlayStream(SimpleExoPlayer _player,String url)
    {
        //url = "https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8";




         if (_player != null)
        {
            // Produces DataSource instances through which media data is loaded.
            dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(getApplicationContext(), getString(R.string.app_name)));
            MediaSource videoSource;
            if(url.contains(".m3u8"))
             videoSource = new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(url));
            else
                videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(url));

            _player.prepare(videoSource);


        }
    }



    private void PlayStreamAds(SimpleExoPlayer _player,String url)
    {

        if (_player != null)
        {


           /* ImaSdkSettings imaSdkSettings = ImaSdkFactory.getInstance().createImaSdkSettings();
            imaSdkSettings.setAutoPlayAdBreaks(true);
            imaSdkSettings.setDebugMode(true);*/




           /* ImaAdsLoader imaAdsLoader = new ImaAdsLoader.Builder(this)
                    .setImaSdkSettings(imaSdkSettings)
                    .setMediaLoadTimeoutMs(30 * 1000)
                    .setVastLoadTimeoutMs(30 * 1000)
                    .buildForAdTag(Uri.parse(adCode));*/  // here is url for vast xml file


            dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(getApplicationContext(), getString(R.string.app_name)));

            //adCode = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/21690435817/rplym/test/test/video&cust_params=test%3D24&optout=false&programma=La prova del cuoco&genere=lifestyle&tipologia=Programmi Tv&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=1533033741271&vid_d=300&allcues=15000";


            MediaSource liveTvMediaSource = getContentMediaSource(url);
             MediaSource mediaSourceWithAds = new AdsMediaSource(
                    liveTvMediaSource,
                    dataSourceFactory,
                    imaAdsLoader,
                    playerView);

            _player.prepare(mediaSourceWithAds);
            _player.setPlayWhenReady(true);
          }
    }

    private MediaSource getContentMediaSource(String url)
    {
        MediaSource videoSource;;
        if(url.contains(".m3u8"))
            videoSource = new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(url));
        else
            videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(url));

        return videoSource;
    }

    @Override
    public void onPause() {
        super.onPause();
//        PausePlayer(player);
        Log("onPause", "");
    }

    @Override
    protected void onResume() {
        super.onResume();
        ResumePlayer(player);
        Log("onResume", "");
    }

    long pausePlayerPosition = 0;

    public void PausePlayer(SimpleExoPlayer _player) {
        // Make sure the playerView stops playing if the user presses the home button.
        if (_player != null) {
            pausePlayerPosition = _player.getCurrentPosition();
            //_player.setPlayWhenReady(false);

            Log("PausePlayer", "");
        }
    }

    final int SEEKING_THRESHOLD = 1;

    public void ResumePlayer(SimpleExoPlayer _player)
    {

            _player.setPlayWhenReady(true);


    }

    @Override
    public void onBackPressed() {

        StopPlayer(player);
        // super.onBackPressed();
    }


    private void StopPlayer(SimpleExoPlayer _player) {
        if (_player != null) {
            _player.stop();
            _player.release();

            _player = null;
            Log("StopPlayer", "Stopped");        }

        PlayerActivity.this.finish();

    }

    private void Log(String source, String log) {
        Log.d("MediaLog" + source, log);
    }




}


