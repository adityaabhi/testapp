package com.abp.loginsample;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;

public class ViewPagerActivity extends AppCompatActivity
{

    ArrayList<String> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        /*Name name = new Name();
        name.userName = "Aditya";
        mList.add(name);
        name.userName = "Android";
        mList.add(name);
        name.userName = "sharma";
        mList.add(name);*/

        mList.add("aditya");
        mList.add("android");
        mList.add("Sharma");


        setContentView(R.layout.activity_view_pager);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager(),mList);
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);


    }
}